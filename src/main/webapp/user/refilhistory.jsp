<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Refil history</title>
</head>
<body>
<table border="1">
    <caption>Accounts</caption>
    <tr>

        <th>Amount</th>
        <th>Purpose</th>
        <th>Time</th>

    </tr>
    <c:forEach items="${requestScope.refHistory}" var="ref">
        <tr>
            <td>${ref.amount}</td>
            <td>${ref.purpose}</td>
            <td>${ref.timestamp}</td></tr>
    </c:forEach>
</table>

<c:forEach  begin="0" end="${pageAmount}" var="i">
 <a href="/user/history?acc=${requestScope.accId}&offset=${i}"/>${i + 1}</a>
</c:forEach>


<p><a href="/user/getAccounts">My Accounts</a> </p>
<p><a href="/logout">Logout</a> </p>

</body>
</html>