<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="/WEB-INF/form.tld"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Accounts</title>
</head>
<body>

<h1><p style="color:red;">${message}</p></h1>

<table border="1">
    <caption>Accounts</caption>
    <tr>
        <th>Account id</th>
        <th>Card number</th>
        <th>Amount</th>
        <th>Maximum_overdraft</th>
        <th>Is blocked</th>
        <th>Block</th>
        <th>History</th>
    </tr>
    <c:forEach items="${accountList}" var="ref">
        <tr><td>${ref.id}</td>
        <td><form:format>${ref.card}</form:format></td>
        <td>${ref.amount}</td>
        <td>${ref.maximumOverdraft}</td>
        <td>${ref.isBlocked}</td>
        <td><a href="/general/blockacc?acc=${ref.id}">Block</a></td>
        <td><a href="/user/history?acc=${ref.id}">History</a></td></tr>
    </c:forEach>
</table>

<p><a href="/user/addnewacc">Add new account.</a></p>


<%--<p><form method="POST" action="/user/refil">
    Account id: <input type="text" name="accId" title="Account id"/><br/>
    Refill summ: <input type="text" name="summ" title="Refil summ"/><br/>
    <input type="submit"/>
</form></p>

<p><form method="POST" action="/user/pay">
    Account id: <input type="text" name="accId" title="Account id"/><br/>
    Refill summ: <input type="text" name="summ" title="Payment Summ"/><br/>
    Requisites: <input type="text" name="requisites" title="requisites"/><br/>
    <input type="submit"/>
</form></p>--%>

<table border="1">
    <caption>Refil</caption>
    <form method="POST" action="/user/refil">
        <tr><th>Account id: </th><th><input type="text" name="accId" title="Account id"/></th></tr>
        <tr><th>Overdraft summ: </th><th><input type="text" name="summ" title="Overdraft summ"/></th><th><input type="submit"/></th></tr>

    </form>
</table>
<table border="1">
    <caption>Pay</caption>
    <form method="POST" action="/user/pay">
        <tr><th>Account id: </th><th><input type="text" name="accId" title="Account id"/></th></tr>
        <tr><th>Payment Summ: </th><th><input type="text" name="summ" title="Payment Summ"/></th></tr>
        <tr><th>Requisites:</th><th> <input type="text" name="requisites" title="requisites"/></th><th><input type="submit"/></th></tr>
    </form>
</table>

<p><a href="/logout">Logout</a> </p>
</body>
</html>
