<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://epam.com/courses/jf/jsp/format" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Accounts</title>
</head>
<body>

<table border="1">
    <caption>Accounts</caption>
    <tr>
        <th>Account id</th>
        <th>Amount</th>
        <th>Maximum_overdraft</th>
        <th>Is blocked</th>
        <th>Block</th>
        <th>Unblock</th>
        <th>Delete</th>
    </tr>
    <c:forEach items="${accountList}" var="ref">
        <tr><td>${ref.id}</td>
            <td>${ref.amount}</td>
            <td>${ref.maximumOverdraft}</td>
            <td>${ref.isBlocked}</td>
            <td><a href="/general/blockacc?acc=${ref.id}">Block</a></td>
            <td><a href="/admin/unblock?acc=${ref.id}">Unblock</a></td>
            <td><a href="/admin/delete?acc=${ref.id}">Delete</a></td></tr>
    </c:forEach>
</table>

<table border="1">
    <caption>Change Overdraft</caption>
    <form method="POST" action="/admin/changeoverdraft">
    <tr><th>Account id: </th><th><input type="text" name="acc" title="Account id"/></th></tr>
        <tr><th>Overdraft summ: </th><th><input type="text" name="summ" title="Overdraft summ"/></th><th><input type="submit"/></th></tr>

    </form>
</table>



<p><a href="/admin/admin.jsp">Admin jsp</a> </p>
<p><a href="/logout">Logout</a> </p>

</body>
</html>
