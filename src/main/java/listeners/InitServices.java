package listeners;

import javax.annotation.Resource;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;
import static services.DbHandler.initDaos;
import static services.GenerateCardNumber.intiGenerator;

@WebListener
public class InitServices implements ServletContextListener {


    @Resource(name = "jdbc/cardservice")
    private static DataSource ds;

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        initDaos(ds);
        intiGenerator();

    }

}
