package services;

import dao.AccountDAO;
import daoImpl.MysqlAccountDAO;

import java.util.Set;

import static services.DbHandler.getDaoByClass;


public class GenerateCardNumber {

    private static Set<Long> set;

    final static long min = 1111_1111_1111_1111L;
    final static long max = 9999_9999_9999_9999L;

    public static long generate(long min, long max){


        return  (long) ((Math.random() * (max - min)) + min);
    }

    public static long generateCardNumber(){

        long temp = 0;
        boolean flag = true;

        while (flag){

            temp = generate(min,max);

            if(set.add(temp))
                flag = false;

        }

        return temp;

    }

    public static void intiGenerator(){

        AccountDAO accountDAO = (MysqlAccountDAO) getDaoByClass(MysqlAccountDAO.class);
        set = accountDAO.getSetOfCards();
    }

}
