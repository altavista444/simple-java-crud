package services;

import dao.Dao;
import daoImpl.MysqlAccountDAO;
import daoImpl.MysqlPersonDAO;
import daoImpl.MysqlRefilHistoryDAO;

import javax.sql.DataSource;

import java.util.HashMap;
import java.util.Map;


public class DbHandler {

    private static Map<Class, Dao> daoMap = new HashMap<>();

    public static void put(Class<? extends Object> clazz, Dao dao) {
        daoMap.put(clazz, dao);
    }

    public static Dao getDaoByClass(Class clazz) {
        return daoMap.get(clazz);
    }

    public static void initDaos(DataSource ds){
            put(MysqlAccountDAO.class, new MysqlAccountDAO(ds));
            put(MysqlPersonDAO.class, new MysqlPersonDAO(ds));
            put(MysqlRefilHistoryDAO.class, new MysqlRefilHistoryDAO(ds));
    }



}
