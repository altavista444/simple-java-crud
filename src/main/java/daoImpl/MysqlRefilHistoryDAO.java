package daoImpl;

import dao.RefilHistoryDao;
import model.RefilHistory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class MysqlRefilHistoryDAO implements RefilHistoryDao {

    DataSource dataSource;

    public MysqlRefilHistoryDAO(DataSource ds){
        dataSource = ds;
    }

    @Override
    public List<RefilHistory> getAllRefilsById(int id,int offset) {

        List<RefilHistory> list = new ArrayList<>();
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT amount,ref_time,purpose FROM refill_history WHERE account_id=? LIMIT ?, 10")){
            preparedStatement.setInt(1,id);
            preparedStatement.setInt(2,offset);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                RefilHistory refilHistory = new RefilHistory();
                refilHistory.setAmount(rs.getInt("amount"));
                refilHistory.setTimestamp(rs.getTimestamp("ref_time"));
                refilHistory.setPurpose(rs.getString("purpose"));
                refilHistory.setAccountId(id);
                list.add(refilHistory);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

    @Override
    public void createRefil(int accID,int amount) {

        createRefil(accID,amount,"Refil");

    }

    @Override
    public void createRefil(int accID, int amount, String purpose) {

        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT into refill_history (account_id,amount,purpose) VALUES (?,?,?)")){
            preparedStatement.setInt(1,accID);
            preparedStatement.setInt(2,amount);
            preparedStatement.setString(3,purpose);
            preparedStatement.executeUpdate();
        }
        catch (SQLException e){
            e.printStackTrace();
        }

    }

    @Override
    public int getMaxNoteNumber(int accId) {

        int amount = 0;

        List<RefilHistory> list = new ArrayList<>();
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("sELECT amount,ref_time,purpose FROM refill_history WHERE account_id=?")){
            preparedStatement.setInt(1, accId);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                amount++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return amount;

    }
}
