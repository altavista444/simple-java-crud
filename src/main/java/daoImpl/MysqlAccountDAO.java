package daoImpl;

import dao.AccountDAO;
import model.Account;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static services.GenerateCardNumber.generateCardNumber;


public class MysqlAccountDAO implements AccountDAO {

    private DataSource dataSource;

    public MysqlAccountDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void insert(int personId) {

        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO account (person_id,card) VALUES (?,?)")){
            preparedStatement.setInt(1,personId);
            preparedStatement.setLong(2,generateCardNumber());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Set<Long> getSetOfCards(){
        Set<Long> set = new HashSet<>();
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT card FROM account")){
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                set.add(rs.getLong("card"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return set;
    }



    @Override
    public List<Account> getAllbyPersonId(int personId) {

        List<Account> accountList = new ArrayList<>();

        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT card,id,amount,maximum_overdraft,is_blocked FROM account WHERE person_id=?")){
            preparedStatement.setInt(1,personId);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                Account account = new Account();
                account.setId(rs.getInt("id"));
                account.setAmount(rs.getInt("amount"));
                account.setMaximumOverdraft(rs.getInt("maximum_overdraft"));
                account.setIsBlocked(rs.getInt("is_blocked"));
                account.setCard(rs.getLong("card"));
                account.setPersonId(personId);
                accountList.add(account);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return accountList;
    }



    @Override
    public void blockAcc(String email,int accId) {

        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE account SET is_blocked=1 where id=? and person_id=(select id from person where email=?)")){
            preparedStatement.setInt(1,accId);
            preparedStatement.setString(2,email);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public String getEmailByAccID(int id){
        String email = null;
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT email FROM person WHERE id=(SELECT person_id FROM account WHERE id=?)")){
            preparedStatement.setInt(1,id);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
              email = rs.getString("email");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return email;
    }


    @Override
    public void changeOverdraft(int accId, int overdraftAmount) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE account SET maximum_overdraft=? WHERE id=?")){
            preparedStatement.setInt(1,overdraftAmount);
            preparedStatement.setInt(2,accId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void updateSumm(int accId, int amount, String email) {

        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE account SET amount=amount+? WHERE id=? and person_id=(SELECT id FROM person WHERE email=?)")){
            preparedStatement.setInt(1,amount);
            preparedStatement.setInt(2,accId);
            preparedStatement.setString(3,email);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Account getAccByID(int accId) {
        Account account = null;

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT card,amount,maximum_overdraft,is_blocked,person_id FROM account WHERE id=?")) {
            preparedStatement.setInt(1,accId);

            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                account = new Account();
                account.setId(accId);
                account.setAmount(rs.getInt("amount"));
                account.setMaximumOverdraft(rs.getInt("maximum_overdraft"));
                account.setIsBlocked(rs.getInt("is_blocked"));
                account.setPersonId(rs.getInt("person_id"));
                account.setCard(rs.getLong("card"));

            }


            return account;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return account;
    }

    @Override
    public void delete(int accId) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM account WHERE id=?")){
            preparedStatement.setInt(1,accId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getMaxId() {
        int id = 0;
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT id from account ORDER BY id DESC LIMIT 1")){

            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                id = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    @Override
    public void unblock(int id) {

        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE account SET is_blocked=0 where id=?")){
            preparedStatement.setInt(1,id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean isBlocked(int accId) {
        int block = -1;

        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT is_blocked FROM account WHERE id=?")){
            preparedStatement.setInt(1,accId);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                block = rs.getInt("is_blocked");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return block == 1;
    }
}
