package daoImpl;

import dao.PersonDAO;
import model.Person;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;



public class MysqlPersonDAO implements PersonDAO {

    private DataSource dataSource;

    public MysqlPersonDAO(DataSource dataSource){
        this.dataSource = dataSource;
    }



    @Override
    public void insert(Person person) {

        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO person (email,password,role_name) VALUES (?,?,?)")){
            preparedStatement.setString(1,person.getEmail());
            preparedStatement.setString(2,person.getPassword());
            preparedStatement.setString(3,person.getRole());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }



    @Override
    public boolean checkDuplicates(Person person) {

        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT email FROM person WHERE email=?")){
            preparedStatement.setString(1,person.getEmail());
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next())
                return true;
        } catch (SQLException e){
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public int getIdByEmail(String email) {
        int id = 0;

        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT id FROM person WHERE email=?")){
            preparedStatement.setString(1,email);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
               id = resultSet.getInt("id");
            }

        } catch (SQLException e){
            e.printStackTrace();
        }

        return id;
    }


}
