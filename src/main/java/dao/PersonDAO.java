package dao;

import model.Person;

public interface PersonDAO extends Dao {

    void insert(Person person);
    boolean checkDuplicates(Person person);
    int getIdByEmail(String email);

}
