package dao;

import model.Account;
import java.util.List;
import java.util.Set;


public interface AccountDAO extends Dao {

    void insert(int personId);
    void changeOverdraft(int accId,int overdraftAmount);
    List<Account> getAllbyPersonId(int id);
    void blockAcc(String email,int accId);
    void updateSumm(int accId, int amount, String email);
    void delete(int accId);
    void unblock(int id);
    String getEmailByAccID(int id);
    Account getAccByID(int AccId);
    boolean isBlocked(int accId);
    Set<Long> getSetOfCards();
    int getMaxId();



}
