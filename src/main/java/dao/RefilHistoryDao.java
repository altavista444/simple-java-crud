package dao;

import model.RefilHistory;

import java.util.List;


public interface RefilHistoryDao extends Dao {

    List<RefilHistory> getAllRefilsById(int id,int offset);
    void createRefil(int accID,int amount);
    void createRefil(int accID,int amount,String purpose);
    int getMaxNoteNumber(int accId);


}
