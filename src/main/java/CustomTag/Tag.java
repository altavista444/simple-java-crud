package CustomTag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;


public class Tag extends SimpleTagSupport {

    StringWriter sw = new StringWriter();

    public void doTag() throws JspException, IOException
    {
        getJspBody().invoke(sw);
        String s = sw.toString();
        JspWriter out = getJspContext().getOut();
        if(!s.equals("")){

            String k =s.substring(0,4) + " " + s.substring(4,8) + " " + s.substring(8,12) + " " + s.substring(12,16);
            out.write(k);
        }
        else
            out.write("");
    }
}
