package model;


public class Person {

    private int id;
    private String email;
    private String password;
    private String role;


    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }


    public Person(int id, String email, String password) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.role = "user";
    }


    public Person(String email, String password, String role) {
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public Person(String email, String password) {
        this.email = email;
        this.password = password;
        this.role = "user";
    }

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }



    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}
