package model;

import java.sql.Timestamp;


public class RefilHistory {

    private int id;
    private int amount;
    private int accountId;
    private Timestamp timestamp;
    private String purpose;

    public void setId(int id) {
        this.id = id;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public int getId() {
        return id;
    }

    public int getAmount() {
        return amount;
    }

    public int getAccountId() {
        return accountId;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
