package model;


public class Account {

    private int id;
    private int personId;
    private int amount;
    private int maximumOverdraft;
    private int isBlocked;
    private long card;

    public long getCard() {
        return card;
    }

    public void setCard(long card) {
        this.card = card;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(int isBlocked) {
        this.isBlocked = isBlocked;
    }

    public int getId() {
        return id;
    }

    public int getPersonId() {
        return personId;
    }

    public int getAmount() {
        return amount;
    }

    public int getMaximumOverdraft() {
        return maximumOverdraft;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setMaximumOverdraft(int maximumOverdraft) {
        this.maximumOverdraft = maximumOverdraft;
    }
}
