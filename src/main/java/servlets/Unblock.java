package servlets;

import dao.AccountDAO;
import dao.PersonDAO;
import daoImpl.MysqlAccountDAO;
import daoImpl.MysqlPersonDAO;
import lombok.extern.log4j.Log4j;
import model.Account;
import model.Person;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;

import static services.DbHandler.getDaoByClass;

@Log4j
@WebServlet("/admin/unblock")
public class Unblock extends HttpServlet {


    private AccountDAO accountDAO;

    @Override
    public void init() throws ServletException {

        accountDAO = (AccountDAO) getDaoByClass(AccountDAO.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        int accForUnblock = Integer.parseInt(req.getParameter("acc"));
        accountDAO.unblock(accForUnblock);
        String email = accountDAO.getEmailByAccID(accForUnblock);
        log.info("Administrator " + req.getRemoteUser() + " unblock account id = " + accForUnblock);
        resp.sendRedirect("/admin/getAccounts?email=" + email);



    }
}
