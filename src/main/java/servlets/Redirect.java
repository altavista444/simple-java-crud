package servlets;

import lombok.extern.log4j.Log4j;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j
@WebServlet("/redirect")
public class Redirect extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if(req.isUserInRole("user"))
            req.getRequestDispatcher("/user/user.jsp").forward(req,resp);

        if(req.isUserInRole("administrator"))
            req.getRequestDispatcher("/admin/admin.jsp").forward(req,resp);

        log.info("User -" + req.getRemoteUser() + " connected");


    }
}
