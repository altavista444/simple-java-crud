package servlets;

import dao.AccountDAO;
import dao.RefilHistoryDao;
import daoImpl.MysqlAccountDAO;
import daoImpl.MysqlRefilHistoryDAO;
import lombok.extern.log4j.Log4j;
import model.Account;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static services.CheckNumber.isNumberandPositive;
import static services.DbHandler.getDaoByClass;

@Log4j
@WebServlet("/user/pay")
public class Pay extends HttpServlet {

    private RefilHistoryDao refilHistoryDao;
    private AccountDAO accountDAO;

    @Override
    public void init() throws ServletException {
        refilHistoryDao = (RefilHistoryDao) getDaoByClass(RefilHistoryDao.class);
        accountDAO = (AccountDAO) getDaoByClass(AccountDAO.class);

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String summ = req.getParameter("summ");
        String accId = req.getParameter("accId");
        String purpose = req.getParameter("requisites");



        if(summ == null || accId == null || purpose == null || !isNumberandPositive(summ) || !isNumberandPositive(accId)){

            req.setAttribute("message","Bad request");
            log.info("Unsuccessful attempt to pay by user" + req.getRemoteUser());
            req.getRequestDispatcher("/user/getAccounts").forward(req,resp);
        }
        else {

            int refillSumm = Integer.parseInt(summ);
            int accountId = Integer.parseInt(accId);
            if(accountId <= accountDAO.getMaxId()) {

                Account account = accountDAO.getAccByID(accountId);

                if ((account.getAmount() + account.getMaximumOverdraft() - refillSumm) < 0 || accountDAO.isBlocked(accountId)) {
                    String error = "No money or account is blocked";
                    log.info("Unsuccessful attempt to pay by user" + req.getRemoteUser());
                    req.getSession().setAttribute("message", error);
                    req.getRequestDispatcher("/user/getAccounts").forward(req, resp);
                } else {
                    refillSumm = -refillSumm;
                    String email = req.getRemoteUser();
                    accountDAO.updateSumm(accountId, refillSumm, email);
                    refilHistoryDao.createRefil(accountId, refillSumm, purpose);
                    req.setAttribute("message","Refil " + -refillSumm + " from account" + accountId);
                    log.info(req.getRemoteUser() + "succesful spent his money. AccountID =" + accountId + ", amount = " + refillSumm);
                    req.getRequestDispatcher("/user/getAccounts").forward(req,resp);
                }
            }
        }








    }
}
