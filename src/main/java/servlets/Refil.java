package servlets;

import dao.AccountDAO;
import dao.RefilHistoryDao;
import daoImpl.MysqlAccountDAO;
import daoImpl.MysqlRefilHistoryDAO;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static services.DbHandler.getDaoByClass;
import static services.CheckNumber.isNumberandPositive;

@Log4j
@WebServlet("/user/refil")
public class Refil extends HttpServlet {

    private RefilHistoryDao refilHistoryDao;
    private AccountDAO accountDAO;

    @Override
    public void init() throws ServletException {
        refilHistoryDao = (RefilHistoryDao) getDaoByClass(RefilHistoryDao.class);
        accountDAO = (AccountDAO) getDaoByClass(AccountDAO.class);

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id = req.getParameter("accId");
        String summ = req.getParameter("summ");


        if (summ == null || id == null || !isNumberandPositive(summ) || !isNumberandPositive(id)) {

            String error = "Please type digit, it must be over than zero";
            req.getSession().setAttribute("message", error);
            log.info("Unsuccessful attempt to pay by user" + req.getRemoteUser());
            req.getRequestDispatcher("/user/getAccounts").forward(req, resp);


        } else {

            int refillSumm = Integer.parseInt(summ);
            int accountId = Integer.parseInt(id);
            String email = req.getRemoteUser();

                    if (accountId <= accountDAO.getMaxId() && accountDAO.getEmailByAccID(accountId).equals(email) && !accountDAO.isBlocked(accountId)) {
                        accountDAO.updateSumm(accountId, refillSumm, email);
                        refilHistoryDao.createRefil(accountId, refillSumm);

                        log.info(req.getRemoteUser() + "succesful give us his money. AccountID =" + accountId + ", amount = " + refillSumm);
                        req.setAttribute("message", "Add " + refillSumm + " to account" + accountId);
                        req.getRequestDispatcher("/user/getAccounts").forward(req, resp);
                    } else {
                        req.setAttribute("message", "Access denied");
                        log.info("Unsuccessful attempt to pay by user" + req.getRemoteUser());
                        req.getRequestDispatcher("/user/getAccounts").forward(req, resp);
                    }


            }
    }




    }

