package servlets;

import dao.PersonDAO;
import daoImpl.MysqlPersonDAO;
import lombok.extern.log4j.Log4j;
import model.Person;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;

import static services.DbHandler.getDaoByClass;

@Log4j
@WebServlet("/registration")
public class Register extends HttpServlet {

    PersonDAO dao;

    @Override
    public void init() throws ServletException {
        dao = (PersonDAO) getDaoByClass(PersonDAO.class);


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String email = req.getParameter("email");
        String pwd = req.getParameter("pass");
        Person person = new Person(email,pwd);

        if(!dao.checkDuplicates(person)) {
            dao.insert(person);
            log.info("We have new registred person. E-mail = " + email);
            req.setAttribute("message","Welcome " + email + ", please login!");
            req.getRequestDispatcher("/index.jsp").forward(req,resp);

        }else {

            req.getSession().setAttribute("message","This email has already been registered");
           req.getRequestDispatcher("/register.jsp").forward(req,resp);
        }




    }
}
