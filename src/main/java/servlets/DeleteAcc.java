package servlets;

import dao.AccountDAO;
import dao.PersonDAO;
import daoImpl.MysqlAccountDAO;
import daoImpl.MysqlPersonDAO;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static services.DbHandler.getDaoByClass;
@Log4j
@WebServlet("/admin/delete")
public class DeleteAcc extends HttpServlet {


    private AccountDAO accountDAO;

    @Override
    public void init() throws ServletException {

        accountDAO = (AccountDAO) getDaoByClass(AccountDAO.class);
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int accForDelete = Integer.parseInt(req.getParameter("acc"));
        String email = accountDAO.getEmailByAccID(accForDelete);
        accountDAO.delete(accForDelete);
        log.info("Administrator " + req.getRemoteUser() + " delete account id = " + accForDelete);
        resp.sendRedirect("/admin/getAccounts?email=" + email);



    }
}
