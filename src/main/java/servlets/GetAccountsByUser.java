package servlets;


import dao.AccountDAO;
import dao.PersonDAO;
import daoImpl.MysqlAccountDAO;
import daoImpl.MysqlPersonDAO;
import lombok.extern.log4j.Log4j;
import model.Account;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import static services.DbHandler.getDaoByClass;
@Log4j
@WebServlet("/user/getAccounts")
public class GetAccountsByUser extends HttpServlet {

    private PersonDAO personDAO;
    private AccountDAO accountDAO;

    @Override
    public void init() throws ServletException {
        personDAO = (PersonDAO) getDaoByClass(PersonDAO.class);
        accountDAO = (AccountDAO) getDaoByClass(AccountDAO.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        int id = personDAO.getIdByEmail(req.getRemoteUser());
        List<Account> list = accountDAO.getAllbyPersonId(id);
        req.setAttribute("accountList", list);
        log.info("User " + req.getRemoteUser() + " get access to his accounts");
        req.getRequestDispatcher("/user/accounts.jsp").forward(req,resp);


    }
}
