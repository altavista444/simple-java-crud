package servlets;

import dao.AccountDAO;
import dao.PersonDAO;
import daoImpl.MysqlAccountDAO;
import daoImpl.MysqlPersonDAO;
import lombok.extern.log4j.Log4j;
import model.Account;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;

import static services.DbHandler.getDaoByClass;

@Log4j
@WebServlet("/admin/getAccounts")
public class GetAccountsByAdmin extends HttpServlet {

    private PersonDAO personDAO;
    private AccountDAO accountDAO;

    @Override
    public void init() throws ServletException {
        personDAO = (PersonDAO) getDaoByClass(PersonDAO.class);
        accountDAO = (AccountDAO) getDaoByClass(AccountDAO.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String pesonEmail = req.getParameter("email");
        int id = personDAO.getIdByEmail(pesonEmail);
        List<Account> list = accountDAO.getAllbyPersonId(id);
        req.setAttribute("accountList", list);
        log.info("Administrator " + req.getRemoteUser() + " change get access to " + pesonEmail + " accounts" );
        req.getRequestDispatcher("/admin/accounts.jsp").forward(req,resp);

    }


}
