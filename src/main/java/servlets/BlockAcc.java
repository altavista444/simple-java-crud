package servlets;

import dao.AccountDAO;
import dao.PersonDAO;
import daoImpl.MysqlAccountDAO;
import daoImpl.MysqlPersonDAO;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


import static services.CheckNumber.isNumberandPositive;
import static services.DbHandler.getDaoByClass;
@Log4j
@WebServlet("/general/blockacc")
public class BlockAcc extends HttpServlet {

    private PersonDAO personDAO;
    private AccountDAO accountDAO;

    @Override
    public void init() throws ServletException {
        personDAO = (PersonDAO) getDaoByClass(PersonDAO.class);
        accountDAO = (AccountDAO) getDaoByClass(AccountDAO.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String param = req.getParameter("acc");


        if(param == null || !isNumberandPositive(param)){

            req.setAttribute("message","Bad request");
            req.getRequestDispatcher("/redirect").forward(req,resp);

        }
        else {

            int accForBlock = Integer.parseInt(param);

            if (!req.isUserInRole("administrator")) {
                accountDAO.blockAcc(req.getRemoteUser(), accForBlock);
                log.info("User " + req.getRemoteUser() + " block account id = " + accForBlock);
                resp.sendRedirect("/user/getAccounts");
            } else {
                log.info("Administrator " + req.getRemoteUser() + " block account id = " + accForBlock);
                String email = accountDAO.getEmailByAccID(accForBlock);
                accountDAO.blockAcc(email, accForBlock);

                resp.sendRedirect("/admin/getAccounts?email=" + email);
            }
        }

    }
}
