package servlets;

import dao.AccountDAO;
import dao.PersonDAO;
import daoImpl.MysqlAccountDAO;
import daoImpl.MysqlPersonDAO;
import lombok.extern.log4j.Log4j;
import services.DbHandler;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;

import static services.DbHandler.getDaoByClass;


@Log4j
@WebServlet("/user/addnewacc")
public class AddNewAccount extends HttpServlet {


    private PersonDAO personDAO;
    private AccountDAO accountDAO;

    @Override
    public void init() throws ServletException {
        personDAO = (PersonDAO) getDaoByClass(PersonDAO.class);
        accountDAO = (AccountDAO) getDaoByClass(AccountDAO.class);
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String email = req.getRemoteUser();

        int userID = personDAO.getIdByEmail(email);
        accountDAO.insert(userID);

        log.info("User " + req.getRemoteUser() + " create new account");

        resp.sendRedirect("/user/getAccounts");
    }
}
