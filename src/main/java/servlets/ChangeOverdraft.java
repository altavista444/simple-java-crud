package servlets;

import dao.AccountDAO;

import daoImpl.MysqlAccountDAO;
import lombok.extern.log4j.Log4j;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static services.DbHandler.getDaoByClass;
@Log4j
@WebServlet("/admin/changeoverdraft")
public class ChangeOverdraft extends HttpServlet {

       private AccountDAO accountDAO;

    @Override
    public void init() throws ServletException {


        accountDAO = (AccountDAO) getDaoByClass(AccountDAO.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        int accId = Integer.parseInt(req.getParameter("acc"));
        int summ = Integer.parseInt(req.getParameter("summ"));
        String email = accountDAO.getEmailByAccID(accId);
        accountDAO.changeOverdraft(accId,summ);
        log.info("Administrator " + req.getRemoteUser() + "change overdtaft on account " + accId + " to " + summ);
        resp.sendRedirect("/admin/getAccounts?email=" + email);

    }
}
