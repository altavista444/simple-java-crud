package servlets;

import dao.AccountDAO;
import dao.PersonDAO;
import dao.RefilHistoryDao;
import daoImpl.MysqlAccountDAO;
import daoImpl.MysqlPersonDAO;
import daoImpl.MysqlRefilHistoryDAO;
import lombok.extern.log4j.Log4j;
import model.RefilHistory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

import static services.DbHandler.getDaoByClass;


@Log4j
@WebServlet("/user/history")
public class GetHistory extends HttpServlet {

    private RefilHistoryDao refilHistoryDao;


    @Override
    public void init() throws ServletException {
        refilHistoryDao = (RefilHistoryDao) getDaoByClass(RefilHistoryDao.class);

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String param = req.getParameter("acc");
        String param2= req.getParameter("offset");
        int offset;
        if (param2 == null)
            offset = 0;
        else
            offset = Integer.parseInt(param2) * 10;

        int accId = Integer.parseInt(param);
        int maxnumber = refilHistoryDao.getMaxNoteNumber(accId);

        List<RefilHistory> refilHistories = refilHistoryDao.getAllRefilsById(accId,offset);
        Integer pageAmount = (maxnumber / 10);


        log.info("User " + req.getRemoteUser() + " open history from account " + accId);
        req.setAttribute("pageAmount",pageAmount);
        req.setAttribute("refHistory",refilHistories);
        req.setAttribute("accId",accId);

        req.getRequestDispatcher("/user/refilhistory.jsp").forward(req,resp);

    }
}
